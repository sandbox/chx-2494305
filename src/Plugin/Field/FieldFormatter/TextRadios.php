<?php

/**
 * @file
 * Contains \Drupal\text_radios\Plugin\Field\FieldFormatter\TextRadios.
 */

namespace Drupal\text_radios\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'text_radios' formatter.
 *
 * @FieldFormatter(
 *   id = "text_radios",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "text_radios",
 *   }
 * )
 */
class TextRadios extends OptionsDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $allowed_values = $this->getFieldSettings()['allowed_values'];
    $header = array_merge([$items->getFieldDefinition()->getLabel()], $allowed_values);
    $allowed_keys = array_keys($allowed_values);
    $length = count($header);
    if ($items->count()) {
      $rows = [];
      foreach ($items as $delta => $item) {
        $table_row = array_fill(0, $length, '');
        $table_row[0] = $item->text_value;
        $table_row[1 + array_search($item->value, $allowed_keys)] = 'X';
        $rows[] = $table_row;
      }
      return [[
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ]];
    }
    return [];
  }
}
