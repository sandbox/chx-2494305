<?php

/**
 * @file
 * Contains \Drupal\text_radios\Plugin\Field\FieldWidget\TextRadios.
 */


namespace Drupal\text_radios\Plugin\Field\FieldWidget;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Radios;
use Drupal\Core\Session\AccountInterface;

/**
 * Plugin implementation of the 'text_radios' widget.
 *
 * @FieldWidget(
 *   id = "text_radios",
 *   label = @Translation("Text + Radios"),
 *   field_types = {
 *     "text_radios",
 *   },
 * )
 */
class TextRadios extends OptionsWidgetBase {

  /**
   * @var array
   */
  protected $trOptions;

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    // Let WidgetBase do all the magic necessary for the widget to work.
    $element = parent::form($items, $form, $form_state, $get_delta);
    // template_preprocess_field_multiple_value_form() creates a draggable
    // table but with the whole widget as a single cell. This is not usable for
    // us.
    unset($element['widget']['#theme']);

    $weight_attributes['class'][] = 'weight';
    // Set up a table.
    $element['widget']['table'] = [
      '#type' => 'table',
      // tableDrag doesn't work if the number of header cells is less than the
      // data cells.
      '#header' => array_merge([$items->getFieldDefinition()->getLabel()], $this->getFieldSettings()['allowed_values'], [t('Weight')]),
      // Move before the Add More button.
      '#weight' => -1,
      '#tabledrag' => [[
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $weight_attributes['class'][0],
      ]],
    ];
    $fake_complete_form = [];
    // Now rearrange the widget into the table rows.
    for ($delta = 0; $delta <= $element['widget']['#max_delta']; $delta++) {
      $single_element = $element['widget'][$delta];
      unset($element['widget'][$delta]);
      $element['widget']['table'][$delta]['#attributes']['class'][] = 'draggable';
      $element['widget']['table'][$delta]['text_value'] = $single_element['text_value'];
      // processRadios does not use form_state nor the complete_form.
      $element['widget']['table'][$delta] += Radios::processRadios($single_element['value'], $form_state, $fake_complete_form);
      // The "weight" key here doesn't matter.
      $element['widget']['table'][$delta]['weight'] = [
        '#type' => 'textfield',
        '#default_value' => $delta,
        '#attributes' => $weight_attributes,
        '#title' => t('Weight for row !delta', ['!delta' => $delta]),
        '#title_display' => 'invisible',
        // WidgetBase::extractFormValues() acts on _weight.
        '#parents' => array_merge($form['#parents'], [$items->getFieldDefinition()->getName(), $delta, '_weight']),
      ];
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $parents = array_merge($form['#parents'], [$items->getFieldDefinition()->getName(), $delta]);
    $item = $items[$delta];
    $entity = $items->getEntity();
    $element['value'] = [
      '#type' => 'radios',
      '#default_value' => isset($item->value) ? $item->value : NULL,
      '#options' => $this->getOptions($entity, 'value'),
      '#parents' => array_merge($parents, ['value']),
      // Radios::processRadios is called outside of the normal codeflow so set
      // default attributes.
      '#attributes' => [],
    ];
    if ($options = $this->getOptions($entity, 'text_value')) {
      $element['text_value'] =  [
        '#type' => 'select',
        '#default_value' => isset($item->text_value) ? $item->text_value : NULL,
        '#options' => $options,
        '#empty_option' => t('Remove this line'),
      ];
    }
    else {
      $element['text_value'] =  [
        '#type' => 'textfield',
        '#default_value' => isset($item->text_value) ? $item->text_value : NULL,
        '#size' => $this->getSetting('size'),
        '#placeholder' => $this->getSetting('placeholder'),
        '#maxlength' => $this->getFieldSetting('max_length'),
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ];
    }
    $element['text_value']['#parents'] = array_merge($parents, ['text_value']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptions(FieldableEntityInterface $entity, $column = NULL) {
    if (!isset($this->trOptions[$column])) {
      unset($this->options);
      if ($column == 'value') {
        $options = array_map(function () { return '';}, parent::getOptions($entity));
      }
      else {
        $options = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('text_allowed_values');
        array_walk_recursive($options, array($this, 'sanitizeLabel'));
      }
      $this->trOptions[$column] = $options;
    }
    return $this->trOptions[$column];
  }

}
