<?php

/**
 * @file
 * Contains \Drupal\text_radios\Plugin\Field\FieldType\TextRadiosItem.
 */


namespace Drupal\text_radios\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;


/**
 * Plugin implementation of the 'text_radios' field type.
 *
 * @FieldType(
 *   id = "text_radios",
 *   label = @Translation("Text radios"),
 *   description = @Translation("This field stores text values plus a value selected by radios"),
 *   category = @Translation("Text"),
 *   default_widget = "text_radios",
 *   default_formatter = "text_radios",
 * )
 */
class TextRadiosItem extends ListIntegerItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'max_length' => 255,
      'text_allowed_values' => [],
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['text_value'] =  DataDefinition::create('string')
      ->setLabel(t('Text value'))
      ->addConstraint('Length', array('max' => 255))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['text_value'] = [
      'type' => 'varchar',
      'length' => $field_definition->getSetting('max_length'),
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    if ($max_length = $this->getSetting('max_length')) {
      $constraints[] = $constraint_manager->create('ComplexData', array(
        'text_value' => array(
          'Length' => array(
            'max' => $max_length,
            'maxMessage' => t('%name: the text may not be longer than @max characters.', array('%name' => $this->getFieldDefinition()->getLabel(), '@max' => $max_length)),
          )
        ),
      ));
    }

    return $constraints;
  }

   /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['max_length'] = array(
      '#type' => 'number',
      '#title' => t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the text part in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    );
    $allowed_values = $this->getSetting('text_allowed_values');
    $element['text_allowed_values'] = array(
      '#type' => 'textarea',
      '#title' => t('Allowed values list'),
      '#default_value' => $this->allowedValuesString($allowed_values),
      '#rows' => 10,
      '#element_validate' => array(array(get_class($this), 'validateTextAllowedValues')),
      '#field_has_data' => $has_data,
      '#field_name' => $this->getFieldDefinition()->getName(),
      '#entity_type' => $this->getEntity()->getEntityTypeId(),
      '#allowed_values' => $allowed_values,
    );
    $element['text_allowed_values']['#description'] = $this->allowedTextValuesDescription();

    return $element;
  }

  /**
   * @param $element
   * @param FormStateInterface $form_state
   */
  public static function validateTextAllowedValues($element, FormStateInterface $form_state) {
    $values = ListStringItem::extractAllowedValues($element['#value'], $element['#field_has_data']);

    if (!is_array($values)) {
      $form_state->setError($element, t('Allowed values list: invalid input.'));
    }
    else {
      // Check that keys are valid for the field type.
      foreach ($values as $key => $value) {
        if ($error = ListStringItem::validateAllowedValue($key)) {
          $form_state->setError($element, $error);
          break;
        }
      }

      // Prevent removing values currently in use.
      if ($element['#field_has_data']) {
        $lost_keys = array_keys(array_diff_key($element['#allowed_values'], $values));
        if (static::textValuesInUse($element['#entity_type'], $element['#field_name'], $lost_keys)) {
          $form_state->setError($element, t('Allowed values list: some values are being removed while currently in use.'));
        }
      }

      $form_state->setValueForElement($element, $values);
    }
  }

  /**
   * @param $entity_type
   * @param $field_name
   * @param $values
   * @return bool
   */
  protected static function textValuesInUse($entity_type, $field_name, $values) {
    return $values && \Drupal::entityQuery($entity_type)
      ->condition($field_name . '.text_value', $values, 'IN')
      ->count()
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
  }

  /**
   * @return string
   */
  protected function allowedTextValuesDescription() {
    $description = '<p>' . t('Leave this empty for free text entry') . '</p>';
    $description .= '<p>' . t('The possible values this field can contain. Enter one value per line, in the format key|label.');
    $description .= '<br/>' . t('The key is the stored value. The label will be used in displayed values and edit forms.');
    $description .= '<br/>' . t('The label is optional: if a line contains a single string, it will be used as key and label.');
    $description .= '</p>';
    $description .= '<p>' . t('Allowed HTML tags in labels: @tags', array('@tags' => $this->displayAllowedTags())) . '</p>';
    return $description;
  }

  /**
   * @inheritdoc.
   */
  public static function storageSettingsFromConfigData(array $settings) {
    $settings = parent::storageSettingsFromConfigData($settings);
    if (isset($settings['text_allowed_values'])) {
      $values = array();
      foreach ($settings['text_allowed_values'] as $item) {
        if (is_array($item['label'])) {
          // Nested elements are embedded in the label.
          $item['label'] = ListStringItem::simplifyAllowedValues($item['label']);
        }
        $values[$item['value']] = $item['label'];
      }
      $settings['text_allowed_values'] = $values;
    }
    return $settings;
  }

  public static function storageSettingsToConfigData(array $settings) {
    $settings = parent::storageSettingsToConfigData($settings);
    if (isset($settings['text_allowed_values'])) {
      $settings['text_allowed_values'] = ListStringItem::structureAllowedValues($settings['text_allowed_values']);
    }
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return !strlen($this->text_value);
  }
}
